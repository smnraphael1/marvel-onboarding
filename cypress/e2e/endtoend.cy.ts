describe('template spec', () => {
  it('passes', () => {
    // Check homepage
    cy.visit('/');
    cy.get('[data-id="nav-characters"]').should('be.visible');
    cy.get('[data-id="nav-comics"]').should('be.visible');
    cy.get('[data-id="nav-series"]').should('be.visible');
    cy.get('[data-id="link-characters"]').should('be.visible');
    cy.get('[data-id="link-comics"]').should('be.visible');
    cy.get('[data-id="link-series"]').should('be.visible');
    cy.get('[data-id="link-characters"]').click();

    // Check characters pages
    cy.get('[data-id="searchbar"]').should('be.visible');
    cy.get('[data-id="search-input"]').type('spider-man');
    cy.get('[data-id="searchbar"]').click();
    cy.get('[data-id="character-card"]').first().click();
    cy.get('[data-id="character-image"]').should('be.visible');
    cy.get('[data-id="character-name"]').should('be.visible');
    cy.get('[data-id="character-description"]').should('be.visible');
    cy.get('[data-id="nav-comics"]').click();

    // Check comics pages
    cy.get('[data-id="searchbar"]').should('be.visible');
    cy.get('[data-id="search-input"]').type('spider-man');
    cy.get('[data-id="searchbar"]').click();
    cy.get('[data-id="comic-card"]').first().click();
    cy.get('[data-id="comic-image"]').should('be.visible');
    cy.get('[data-id="comic-title"]').should('be.visible');
    cy.get('[data-id="comic-description"]').should('be.visible');
    cy.get('[data-id="nav-series"]').click();

    // Check series pages
    cy.get('[data-id="searchbar"]').should('be.visible');
    cy.get('[data-id="search-input"]').type('spider-man');
    cy.get('[data-id="searchbar"]').click();
    cy.get('[data-id="serie-card"]').first().click();
    cy.get('[data-id="serie-image"]').should('be.visible');
    cy.get('[data-id="serie-title"]').should('be.visible');
    cy.get('[data-id="serie-description"]').should('be.visible');

    // Return to homepage
    cy.get('[data-id="homepage-link"]').click();
  });
});
