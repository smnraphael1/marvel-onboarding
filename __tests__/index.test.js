import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import Home from '../src/app/page.tsx';
import Navbar from '../src/app/components/Navbar.tsx';
import NotFound from '../src/app/not-found.tsx';

describe('Home', () => {
  it('renders a heading', () => {
    render(<Home />);

    const heading = screen.getByRole('heading', { level: 1 });

    expect(heading).toBeInTheDocument();
    expect(heading).toHaveTextContent('Welcome to Marvel App');
  });

  it('renders navigation links', () => {
    render(<Home />);

    const charactersLink = screen.getByText('Characters');
    const comicsLink = screen.getByText('Comics');
    const seriesLink = screen.getByText('Series');

    expect(charactersLink).toBeInTheDocument();
    expect(comicsLink).toBeInTheDocument();
    expect(seriesLink).toBeInTheDocument();
  });
});

describe('Navbar', () => {
  it('renders navigation link', () => {
    render(<Navbar />);

    const charactersLink = screen.getByText('Characters');
    const comicsLink = screen.getByText('Comics');
    const seriesLink = screen.getByText('Series');

    expect(charactersLink).toBeInTheDocument();
    expect(comicsLink).toBeInTheDocument();
    expect(seriesLink).toBeInTheDocument();
  });
});

describe('Not Found', () => {
  it('renders a heading', () => {
    render(<NotFound />);

    const heading = screen.getByRole('heading', { level: 2 });

    expect(heading).toBeInTheDocument();
    expect(heading).toHaveTextContent('Not Found');
  });

  it('renders navigation link', () => {
    render(<NotFound />);

    const homeLink = screen.getByText('Return Home');

    expect(homeLink).toBeInTheDocument();
  });
});
