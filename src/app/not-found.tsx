import Link from 'next/link';

export default function NotFound() {
  return (
    <div className='flex flex-col gap-2 items-center justify-center'>
      <h2>Not Found</h2>
      <p>Could not find requested resource</p>
      <Link href='/' className='bg-red-100 p-2 rounded-lg'>
        Return Home
      </Link>
    </div>
  );
}
