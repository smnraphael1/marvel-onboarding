import Link from 'next/link';

type Serie = {
  id: number;
  title: string;
  thumbnail: {
    path: string;
    extension: string;
  };
};

function SeriesCard({ id, title, thumbnail }: Serie) {
  return (
    <div className='w-48'>
      <Link href={`/series/${id}`} data-id='serie-card'>
        <div className='flex flex-col items-center gap-2 hover:text-red-600'>
          <img
            src={`${thumbnail.path}.${thumbnail.extension}`}
            className='h-48 w-full object-cover'
          />
          <p className='text-sm font-medium'>{title}</p>
        </div>
      </Link>
    </div>
  );
}

export default SeriesCard;
