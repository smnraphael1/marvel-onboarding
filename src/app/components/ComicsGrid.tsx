import React, { useEffect, useState } from 'react';
import ComicsCard from './ComicsCard';
import { getComics } from '../utils/api';
import Loader from './Loader';
import { motion } from 'framer-motion';
import { Comic } from '../types/types';

function ComicsGrid() {
  const [comics, setComics] = useState<Comic[]>([]);
  const [loading, setLoading] = useState(true);
  const [searchQuery, setSearchQuery] = useState('');

  const fetchComics = async (nameStartsWith?: string | null) => {
    setLoading(true);
    try {
      const response = await getComics(nameStartsWith);
      setComics(response.data.results);
    } catch (error) {
      console.error('Error fetching comics:', error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchComics();
  }, []);

  const handleSearchClick = () => {
    fetchComics(searchQuery || undefined);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };

  return (
    <div className='flex flex-col items-center gap-10'>
      <div className='flex gap-5'>
        <input
          type='text'
          placeholder='Search characters...'
          value={searchQuery}
          onChange={handleInputChange}
          className='border border-red-600 p-2'
          data-id='search-input'
        />
        <button
          onClick={handleSearchClick}
          className='text-white bg-red-600 font-bold p-2'
          data-id='searchbar'
        >
          SEARCH
        </button>
      </div>
      {loading ? (
        <Loader />
      ) : (
        <div className='grid grid-cols-5 gap-x-5 gap-y-10'>
          {comics.map((comic) => (
            <motion.div
              initial={{ rotate: -5, scale: 0.5 }}
              animate={{ rotate: 0, scale: 1 }}
              whileHover={{ scale: 1.1 }}
              transition={{
                type: 'spring',
                stiffness: 260,
                damping: 25
              }}
              key={comic.id}
            >
              <ComicsCard id={comic.id} title={comic.title} thumbnail={comic.thumbnail} />
            </motion.div>
          ))}
        </div>
      )}
    </div>
  );
}

export default ComicsGrid;
