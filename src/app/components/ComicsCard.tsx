import Link from 'next/link';

type Comic = {
  id: number;
  title: string;
  thumbnail: {
    path: string;
    extension: string;
  };
};

function ComicsCard({ id, title, thumbnail }: Comic) {
  return (
    <div className='w-48'>
      <Link href={`/comics/${id}`} data-id='comic-card'>
        <div className='flex flex-col items-center gap-2 hover:text-red-600'>
          <img
            src={`${thumbnail.path}.${thumbnail.extension}`}
            className='h-48 w-full object-cover'
          />
          <p className='text-sm font-medium'>{title}</p>
        </div>
      </Link>
    </div>
  );
}

export default ComicsCard;
