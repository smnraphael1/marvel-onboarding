import Link from 'next/link';
import Image from 'next/image';

function Navbar() {
  return (
    <div className='flex justify-between bg-red-50 p-5'>
      <Link href='/' className='text-xl font-bold' data-id='homepage-link'>
        <Image src='/marvel_logo.png' width={124} height={50} alt='Marvel logo' />
      </Link>
      <ul className='flex gap-5 items-center font-medium'>
        <li>
          <Link href='/characters' className='hover:text-red-600' data-id='nav-characters'>
            Characters
          </Link>
        </li>
        <li>
          <Link href='/comics' className='hover:text-red-600' data-id='nav-comics'>
            Comics
          </Link>
        </li>
        <li>
          <Link href='/series' className='hover:text-red-600' data-id='nav-series'>
            Series
          </Link>
        </li>
      </ul>
    </div>
  );
}

export default Navbar;
