import Link from 'next/link';

type Character = {
  id: number;
  name: string;
  thumbnail: {
    path: string;
    extension: string;
  };
};

function CharactersCard({ id, name, thumbnail }: Character) {
  return (
    <div className='w-48'>
      <Link href={`/characters/${id}`} data-id='character-card'>
        <div className='flex flex-col items-center gap-2 hover:text-red-600'>
          <img
            src={`${thumbnail.path}.${thumbnail.extension}`}
            className='h-48 w-full object-cover'
          />
          <p className='text-sm font-medium'>{name}</p>
        </div>
      </Link>
    </div>
  );
}

export default CharactersCard;
