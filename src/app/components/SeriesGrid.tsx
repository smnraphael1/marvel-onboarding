import React, { useEffect, useState } from 'react';
import SeriesCard from './SeriesCard';
import { getSeries } from '../utils/api';
import Loader from './Loader';
import { motion } from 'framer-motion';
import { Serie } from '../types/types';

function SeriesGrid() {
  const [series, setSeries] = useState<Serie[]>([]);
  const [loading, setLoading] = useState(true);
  const [searchQuery, setSearchQuery] = useState('');

  const fetchSeries = async (nameStartsWith?: string | null) => {
    setLoading(true);
    try {
      const response = await getSeries(nameStartsWith);
      setSeries(response.data.results);
    } catch (error) {
      console.error('Error fetching series:', error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchSeries();
  }, []);

  const handleSearchClick = () => {
    fetchSeries(searchQuery || undefined);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };

  return (
    <div className='flex flex-col items-center gap-10'>
      <div className='flex gap-5'>
        <input
          type='text'
          placeholder='Search characters...'
          value={searchQuery}
          onChange={handleInputChange}
          className='border border-red-600 p-2'
          data-id='search-input'
        />
        <button
          onClick={handleSearchClick}
          className='text-white bg-red-600 font-bold p-2'
          data-id='searchbar'
        >
          SEARCH
        </button>
      </div>
      {loading ? (
        <Loader />
      ) : (
        <div className='grid grid-cols-5 gap-x-5 gap-y-10'>
          {series.map((serie) => (
            <motion.div
              initial={{ rotate: -5, scale: 0.5 }}
              animate={{ rotate: 0, scale: 1 }}
              whileHover={{ scale: 1.1 }}
              transition={{
                type: 'spring',
                stiffness: 260,
                damping: 25
              }}
              key={serie.id}
            >
              <SeriesCard id={serie.id} title={serie.title} thumbnail={serie.thumbnail} />
            </motion.div>
          ))}
        </div>
      )}
    </div>
  );
}

export default SeriesGrid;
