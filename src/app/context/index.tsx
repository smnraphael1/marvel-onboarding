// 'use client';

// import React, { createContext, useContext, useState } from 'react';

// type ComicsData = {
//   data: {
//     results: {
//       id: number;
//       title: string;
//       thumbnail: {
//         path: string;
//         extension: string;
//       };
//     }[];
//   };
// };

// type CharactersData = {
//   data: {
//     results: {
//       id: number;
//       name: string;
//       thumbnail: {
//         path: string;
//         extension: string;
//       };
//     }[];
//   };
// };

// type SeriesData = {
//   data: {
//     results: {
//       id: number;
//       title: string;
//       thumbnail: {
//         path: string;
//         extension: string;
//       };
//     }[];
//   };
// };

// type ContextType = {
//   comicsData: ComicsData | null;
//   setComicsData: React.Dispatch<React.SetStateAction<ComicsData | null>>;
//   charactersData: CharactersData | null;
//   setCharactersData: React.Dispatch<React.SetStateAction<CharactersData | null>>;
//   seriesData: SeriesData | null;
//   setSeriesData: React.Dispatch<React.SetStateAction<SeriesData | null>>;
// };

// const AppContext = createContext<ContextType | null>(null);

// export function AppWrapper({ children }: { children: React.ReactNode }) {
//   const [comicsData, setComicsData] = useState<ComicsData | null>(null);
//   const [charactersData, setCharactersData] = useState<CharactersData | null>(null);
//   const [seriesData, setSeriesData] = useState<SeriesData | null>(null);

//   return (
//     <AppContext.Provider
//       value={{
//         comicsData,
//         setComicsData,
//         charactersData,
//         setCharactersData,
//         seriesData,
//         setSeriesData
//       }}
//     >
//       {children}
//     </AppContext.Provider>
//   );
// }

// export function useAppContext() {
//   return useContext(AppContext);
// }
