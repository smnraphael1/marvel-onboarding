'use client';

import ComicsGrid from '../components/ComicsGrid';

function Page() {
  return (
    <div className='flex flex-col items-center pb-10'>
      <h1 className='text-3xl font-semibold my-10'>Comics</h1>
      <ComicsGrid />
    </div>
  );
}

export default Page;
