'use client';

import { useEffect, useState } from 'react';
import { getOneComic } from '../../utils/api';
import Link from 'next/link';
import Loader from '@/app/components/Loader';
import { Comic } from '@/app/types/types';

type ComicsPageProps = {
  params: {
    comicId: string;
  };
};

function Page({ params }: ComicsPageProps) {
  const [oneComic, setOneComic] = useState<Comic | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getOneComic(params.comicId)
      .then((response) => {
        console.log(response.data.results[0]);
        setOneComic(response.data.results[0]);
        setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching comic:', error);
        setLoading(false);
      });
  }, []);

  return (
    <div className='flex flex-col gap-5 items-center justify-center py-10 px-48'>
      {loading || !oneComic ? (
        <Loader />
      ) : (
        <div className='flex flex-col items-start justify-center gap-3 w-9/12'>
          <div className='flex items-start gap-3 border rounded-lg p-2 w-full'>
            <img
              src={`${oneComic.thumbnail.path}.${oneComic.thumbnail.extension}`}
              className='h-64 w-64 self-center'
              alt={oneComic.title}
              data-id='comic-image'
            />
            <div className='flex flex-col gap-5'>
              <h1 data-id='comic-title'>
                <span className='font-bold'>Title: </span>
                {oneComic.title}
              </h1>
              <p data-id='comic-description'>
                <span className='font-bold'>Description: </span>
                {oneComic.description ? oneComic.description : 'N/A'}
              </p>
            </div>
          </div>
          <div className='flex flex-col gap-2 border rounded-lg p-2 w-full'>
            <h1 className='text-lg font-semibold'>Characters</h1>
            <div>
              {oneComic.characters.items.map((comic, index) => (
                <div key={index}>
                  <Link
                    href={`/characters/${comic.resourceURI.split('http://gateway.marvel.com/v1/public/characters/').pop()}`}
                    className='hover:text-red-600'
                  >
                    - {comic.name}
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Page;
