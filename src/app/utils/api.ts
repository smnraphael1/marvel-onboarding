export const getCharacters = async (nameStartsWith?: string | null) => {
  const nameStartsWithParam = nameStartsWith !== undefined ? nameStartsWith : null;
  const url =
    `https://gateway.marvel.com/v1/public/characters?ts=1&apikey=8b473e56697da1f5d1e72cd57da7b03e&hash=9848df2124d8299adacc27d125de7461&limit=50` +
    (nameStartsWithParam !== null ? `&nameStartsWith=${nameStartsWithParam}` : '');
  const response = await fetch(url);
  const data = response.json();
  return data;
};

export const getOneCharacter = async (characterId: string) => {
  const url = `https://gateway.marvel.com/v1/public/characters/${characterId}?ts=1&apikey=8b473e56697da1f5d1e72cd57da7b03e&hash=9848df2124d8299adacc27d125de7461`;
  const response = await fetch(url);
  const data = response.json();
  return data;
};

export const getComics = async (titleStartsWith?: string | null) => {
  const titleStartsWithParam = titleStartsWith !== undefined ? titleStartsWith : null;
  const url =
    `https://gateway.marvel.com/v1/public/comics?ts=1&apikey=8b473e56697da1f5d1e72cd57da7b03e&hash=9848df2124d8299adacc27d125de7461&limit=50` +
    (titleStartsWithParam !== null ? `&titleStartsWith=${titleStartsWithParam}` : '');
  const response = await fetch(url);
  const data = response.json();
  return data;
};

export const getOneComic = async (comicId: string) => {
  const url = `https://gateway.marvel.com/v1/public/comics/${comicId}?ts=1&apikey=8b473e56697da1f5d1e72cd57da7b03e&hash=9848df2124d8299adacc27d125de7461`;
  const response = await fetch(url);
  const data = response.json();
  return data;
};

export const getSeries = async (titleStartsWith?: string | null) => {
  const titleStartsWithParam = titleStartsWith !== undefined ? titleStartsWith : null;
  const url =
    'https://gateway.marvel.com/v1/public/series?ts=1&apikey=8b473e56697da1f5d1e72cd57da7b03e&hash=9848df2124d8299adacc27d125de7461&limit=50' +
    (titleStartsWithParam !== null ? `&titleStartsWith=${titleStartsWithParam}` : '');
  const response = await fetch(url);
  const data = response.json();
  return data;
};

export const getOneSerie = async (serieId: string) => {
  const url = `https://gateway.marvel.com/v1/public/series/${serieId}?ts=1&apikey=8b473e56697da1f5d1e72cd57da7b03e&hash=9848df2124d8299adacc27d125de7461`;
  const response = await fetch(url);
  const data = response.json();
  return data;
};
