export type Character = {
  id: number;
  name: string;
  description: string;
  thumbnail: {
    path: string;
    extension: string;
  };
  comics: {
    items: {
      name: string;
      resourceURI: string;
    }[];
  };
  series: {
    items: {
      name: string;
      resourceURI: string;
    }[];
  };
};

export type Comic = {
  id: number;
  title: string;
  description: string;
  thumbnail: {
    path: string;
    extension: string;
  };
  characters: {
    items: {
      name: string;
      resourceURI: string;
    }[];
  };
};

export type Serie = {
  id: number;
  title: string;
  description: string;
  thumbnail: {
    path: string;
    extension: string;
  };
  characters: {
    items: {
      name: string;
      resourceURI: string;
    }[];
  };
};
