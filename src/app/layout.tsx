import React from 'react';
import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
// import { AppWrapper } from './context';
import Navbar from './components/Navbar';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Marvel Project',
  description: 'Marvel Project'
};

export default function RootLayout({
  children
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang='en'>
      {/* <AppWrapper> */}
      <body className={inter.className}>
        <Navbar />
        {children}
      </body>
      {/* </AppWrapper> */}
    </html>
  );
}
