'use client';

import CharactersGrid from '../components/CharactersGrid';

function Page() {
  return (
    <div className='flex flex-col items-center pb-10'>
      <h1 className='text-3xl font-semibold my-10'>Characters</h1>
      <CharactersGrid />
    </div>
  );
}

export default Page;
