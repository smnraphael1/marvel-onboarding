'use client';

import { useEffect, useState } from 'react';
import { getOneCharacter } from '../../utils/api';
import Link from 'next/link';
import Loader from '@/app/components/Loader';
import { Character } from '@/app/types/types';

type CharactersPageProps = {
  params: {
    characterId: string;
  };
};

function Page({ params }: CharactersPageProps) {
  const [oneCharacter, setOneCharacter] = useState<Character | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getOneCharacter(params.characterId)
      .then((response) => {
        console.log(response.data.results[0]);
        setOneCharacter(response.data.results[0]);
        setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching character:', error);
        setLoading(false);
      });
  }, []);

  return (
    <div className='flex flex-col gap-5 items-center justify-center py-10 px-48'>
      {loading || !oneCharacter ? (
        <Loader />
      ) : (
        <div className='flex flex-col items-start justify-center gap-3 w-9/12'>
          <div className='flex items-start gap-3 border rounded-lg p-2 w-full'>
            <img
              src={`${oneCharacter.thumbnail.path}.${oneCharacter.thumbnail.extension}`}
              className='h-64 w-64 self-center'
              alt={oneCharacter.name}
              data-id='character-image'
            />
            <div className='flex flex-col gap-5'>
              <h1 data-id='character-name'>
                <span className='font-bold'>Name: </span>
                {oneCharacter.name}
              </h1>
              <p data-id='character-description'>
                <span className='font-bold'>Description: </span>{' '}
                {oneCharacter.description ? oneCharacter.description : 'N/A'}
              </p>
            </div>
          </div>
          <div className='flex flex-col gap-2 border rounded-lg p-2 w-full'>
            <h1 className='text-lg font-semibold'>Comics</h1>
            <div>
              {oneCharacter.comics.items.map((comic, index) => (
                <div key={index}>
                  <Link
                    href={`/comics/${comic.resourceURI.split('http://gateway.marvel.com/v1/public/comics/').pop()}`}
                    className='hover:text-red-600'
                  >
                    - {comic.name}
                  </Link>
                </div>
              ))}
            </div>
          </div>
          <div className='flex flex-col gap-2 border rounded-lg p-2 w-full'>
            <h1 className='text-lg font-semibold'>Series</h1>
            <div>
              {oneCharacter.series.items.map((serie, index) => (
                <div key={index}>
                  <Link
                    href={`/series/${serie.resourceURI.split('http://gateway.marvel.com/v1/public/series/').pop()}`}
                    className='hover:text-red-600'
                  >
                    - {serie.name}
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Page;
