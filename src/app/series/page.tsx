'use client';

import SeriesGrid from '../components/SeriesGrid';

function Page() {
  return (
    <div className='flex flex-col items-center pb-10'>
      <h1 className='text-3xl font-semibold my-10'>Series</h1>
      <SeriesGrid />
    </div>
  );
}

export default Page;
