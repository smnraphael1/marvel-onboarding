'use client';

import { getOneSerie } from '@/app/utils/api';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import Loader from '@/app/components/Loader';
import { Serie } from '@/app/types/types';

type SeriesPageProps = {
  params: {
    serieId: string;
  };
};

function Page({ params }: SeriesPageProps) {
  const [oneSerie, setOneSerie] = useState<Serie | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getOneSerie(params.serieId)
      .then((response) => {
        console.log(response.data.results[0]);
        setOneSerie(response.data.results[0]);
        setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching serie:', error);
        setLoading(false);
      });
  }, []);

  return (
    <div className='flex flex-col gap-5 items-center justify-center py-10 px-48'>
      {loading || !oneSerie ? (
        <Loader />
      ) : (
        <div className='flex flex-col items-start justify-center gap-3 w-9/12'>
          <div className='flex items-start gap-3 border rounded-lg p-2 w-full'>
            <img
              src={`${oneSerie.thumbnail.path}.${oneSerie.thumbnail.extension}`}
              className='h-64 w-64 self-center'
              alt={oneSerie.title}
              data-id='serie-image'
            />

            <div className='flex flex-col gap-5'>
              <h1 data-id='serie-title'>
                <span className='font-bold'>Title: </span>
                {oneSerie.title}
              </h1>
              <p data-id='serie-description'>
                <span className='font-bold'>Description: </span>
                {oneSerie.description ? oneSerie.description : 'N/A'}
              </p>
            </div>
          </div>
          <div className='flex items-start gap-3 border rounded-lg p-2 w-full'>
            <h1 className='text-lg font-semibold'>Characters</h1>
            <div>
              {oneSerie.characters.items.map((serie, index) => (
                <div key={index}>
                  <Link
                    href={`/characters/${serie.resourceURI.split('http://gateway.marvel.com/v1/public/characters/').pop()}`}
                    className='hover:text-red-600'
                  >
                    - {serie.name}
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Page;
