import Link from 'next/link';

export default function Home() {
  return (
    <main className='h-screen flex flex-col gap-10 items-center mt-52'>
      <h1 className='text-3xl font-bold'>Welcome to Marvel App</h1>
      <p>Here you can access you favorite:</p>
      <div className='flex gap-5 font-bold'>
        <Link href='/characters' className='hover:text-red-600' data-id='link-characters'>
          Characters
        </Link>
        <Link href='/comics' className='hover:text-red-600' data-id='link-comics'>
          Comics
        </Link>
        <Link href='/series' className='hover:text-red-600' data-id='link-series'>
          Series
        </Link>
      </div>
      <footer className='absolute bottom-0 p-5 bg-red-100 w-full flex justify-center'>
        <p className='italic text-sm'>Data provided by Marvel. © 2016 Marvel.</p>
      </footer>
    </main>
  );
}
